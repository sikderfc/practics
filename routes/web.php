<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FormController;
use App\Http\Controllers\CourseController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\StudentController;
use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dashboard');
});

Route:: get('/products', [ProductController::class, 'index'])->name('products.index');
Route:: get('/products.show/{id}', [ProductController::class, 'show'])->name('products.show');
Route:: get('/products.destroy/{id}', [ProductController::class, 'destroy'])->name('products.destroy');



//Students Route Start
Route:: get('/students', [StudentController::class, 'index'])->name('students.index');
Route:: get('/student_create', [StudentController::class, 'create'])->name('students.create');
Route:: post('/student_store', [StudentController::class, 'store'])->name('students.store');
Route:: get('/student_show/{id}', [StudentController::class, 'show'])->name('students.show');
Route:: get('/student_edit/{id}', [StudentController::class, 'edit'])->name('students.edit');
Route:: post('/student_update/{id}', [StudentController::class, 'update'])->name('students.update');
Route:: get('/student_delete/{id}', [StudentController::class, 'destroy'])->name('students.destroy');
// .END Students

//courses start

Route:: get('/course', [CourseController::class, 'index'])->name('course.index');
Route:: get('/course_create', [CourseController::class, 'create'])->name('courese.create');
Route:: post('/course_store', [CourseController::class, 'store'])->name('courese.store');
Route:: get('/course_edit/{id}', [CourseController::class, 'edit'])->name('courese.edit');
Route:: post('/course_update/{id}', [CourseController::class, 'update'])->name('courese.update');


Route:: get('/form',[FormController::class, 'index'] )->name('form.index');
Route:: get('form_create', [FormController::class, 'create'])->name('form.create');
Route:: post('form_store', [FormController::class, 'store'])->name('form.store');
Route:: get('form_show/{id}', [FormController::class, 'show'])->name('form.show');
Route:: get('form_edit/{id}', [FormController::class, 'edit'])->name('form.edit');
Route:: post('form_update/{id}', [FormController::class, 'update'])->name('form.update');
Route:: get('form_destroy/{id}', [FormController::class, 'destroy'])->name('form.destroy');