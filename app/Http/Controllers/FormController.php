<?php

namespace App\Http\Controllers;

use App\Models\Form;
use Illuminate\Http\Request;

class FormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $formData = form::all() ;
        return view('form.index',compact('formData'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    
        return view('form.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $extension = $request->file('imge')->getClientOriginalExtension();
        //$extension = $request->file('imge')->getClientOriginalExtension();
        $full_extension = $request->file('imge')->getClientOriginalName();
        $fileName = date('y-m-d').time().$full_extension ;
        // $request->file('imge')->store('/public/form_image/'.$fileName);        
        $request->file('imge')->move(storage_path('/app/public/form_image'),$fileName);        
       
      $requestData =[
        'email'=> $request->email,
        'password'=> $fileName,
        'imge'=> $request->password,
        'checkbox'=> $request->checkbox,
        'sex'=> $request->sex,
        'selectbox'=> $request->selectbox,
        'is_active'=> $request->is_active ? true : false,
      ];


      $checkbox = $requestData['checkbox'];
       $requestData ['checkbox'] = implode(',', $checkbox);
// dd($requestData);
      form::insert($requestData);
      return redirect()
        ->route('form.index')
        ->withMessage('Successfully Created');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Form  $form
     * @return \Illuminate\Http\Response
     */
    public function show($form)
    {
        $formList = form::find($form);
        return view('form.show',compact('formList'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Form  $form
     * @return \Illuminate\Http\Response
     */
    public function edit($form)
    {
        $formGetdata = form::find($form);
        $formGetdata['checkbox'] = explode(',', $formGetdata->checkbox);

        return view('form.edit',compact('formGetdata'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Form  $form
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$form)
    {
        $formList = form::find($form);

        $requestData =[
            'email'=> $request->email,
            'password'=> $request->password,
            'checkbox'=> $request->checkbox,
            'sex'=> $request->sex,
            'selectbox'=> $request->selectbox,
            'is_active'=> $request->is_active ? true : false,
          ];
          $checkbox = $requestData['checkbox'];
          $requestData ['checkbox'] = implode(',', $checkbox);
    
          $formList->update($requestData);
          return redirect()
            ->route('form.index')
            ->withMessage('Successfully updated ');    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Form  $form
     * @return \Illuminate\Http\Response
     */
    public function destroy($form)
    {
        $formList = form::find($form);
        $formList->delete();
        return redirect()
            ->route('form.index')
            ->withMessage('Successfully delete ');    }
   
            
}
