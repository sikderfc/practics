<?php

namespace App\Http\Controllers;

use App\Http\Requests\StudentRequest;
use App\Models\Student;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $student_info = Student::all();
        return view('students/index',compact('student_info'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('students/create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StudentRequest $request)
    {
      
        //Data Receive
        $requestData =
            [
                'name' => $request->name,
                'dob' => $request->dob,
                'gender' => $request->gender,
                'hobies' => $request->hobies,
                'nationality' => $request->nationality,
            ];
            $hobies = $requestData['hobies'];
            $requestData['hobies'] = implode(',',$hobies);
        Student::create($requestData);
        return redirect()
        ->route('students.index')
        ->withMessage('Successfully Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        dd($student);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit($student)
    {
        $student_info_Byid =Student::find($student);
        $student_info_Byid['hobies'] = explode(',', $student_info_Byid->hobies);
// dd($student_info_Byid);

        return view('students/edit',compact('student_info_Byid'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $studentList = Student::find($id);
       
        $requestData =
            [
                'name' => $request->name,
                'dob' => $request->dob,
                'gender' => $request->gender,
                'hobies' => $request->hobies,
                'nationality' => $request->nationality,
            ];
            $hobies = $requestData['hobies'];
            $requestData['hobies'] = implode(',',$hobies);
           
            $studentList->update($requestData);
        return redirect()
        ->route('students.index')
        ->withMessage('Successfully Updated');
    }   

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
        //
    }
}
