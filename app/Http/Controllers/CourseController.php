<?php

namespace App\Http\Controllers;

use App\Models\course;
use Illuminate\Http\Request;
use App\Http\Requests\CourseRequest;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courseInfo =course::all() ;
    
        return view('courses/index',compact('courseInfo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('courses/create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CourseRequest $request)
    {
        $requestData = [
            'title' => $request->title,
            'category' => $request->category,
            'type' => $request->type,
            'technology' => $request->technology,
            'duraton' => $request->duraton,
            'start_form' => $request->start_form,
        ];
        $technology = $requestData['technology'];
       $requestData['technology'] = implode(',',$technology);
    //    dd($requestData);
        course::create($requestData);
        return redirect()
            ->route('courese.create')
            ->withMessage('Successfully Created');
        ;


    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\course  $course
     * @return \Illuminate\Http\Response
     */
    public function show(course $course)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\course  $course
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $courseInfo =course::find($id);
        $courseInfo['technology'] = explode(',', $courseInfo->technology);
        
        // $courseInfo =course::find($id);
        // $courseInfo['technology'] = explode(',', $courseInfo->technology);

        //$student_info_Byid['hobies'] = explode(',', $student_info_Byid->hobies);

        return view('courses/edit',compact('courseInfo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\course  $course
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $CourseList = course::find($id);

        $requestData = [
            'title' => $request->title,
            'category' => $request->category,
            'type' => $request->type,
            'technology' => $request->technology,
            'duraton' => $request->duraton,
            'start_form' => $request->start_form,
        ];
       
        $technology = $requestData['technology'];
       $requestData['technology'] = implode(',',$technology);
    //    dd($requestData);
    $CourseList->update($requestData);

        return redirect()
            ->route('courese.create')
            ->withMessage('Successfully Created');
        ;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\course  $course
     * @return \Illuminate\Http\Response
     */
    public function destroy(course $course)
    {
        //
    }
}
