@props(['name','title','type','id'])

<div class="form-group row">
        <label for="{{$id}}"  class="col-sm-2 col-form-label">{{ucwords($title)}}</label>
        <div class="col-sm-6">
            <input  type="{{$type}}" name="{{$name}}" id="{{$id}}" {{$attributes->merge(['class' =>'form-control'])}}  >
        </div>
    </div>