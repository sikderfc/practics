<x-master>
    <div class="d-flex m-5 justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Edit Student</h1>

    </div>
    <div class=" p-4 m-2" style="width:50%">
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
    <form action="{{Route('students.update',$student_info_Byid->id)}}" method="POST">
        @csrf   
        <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label"> Student Name</label>
                <div class="col-sm-10">
                    <input type="text" value="{{$student_info_Byid->name}}" name ="name" class="form-control" id="inputEmail3" placeholder="Enter Your Name">
                </div>
            </div>
            <br>
            <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label">Date of Birth</label>
                <div class="col-sm-10">
                    <input type="text" value="{{$student_info_Byid->dob}}" name ="dob" class="form-control" id="inputEmail3" placeholder="Enter Your Date Of Birth">
                </div>
            </div>
            <br>

            <fieldset class="form-group">
                <div class="row">
                    <legend class="col-form-label col-sm-2 pt-0">Gender</legend>
                    <div class="col-sm-10">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="gender" id="gender1" value="Male" {{$student_info_Byid->gender == 'Male' ? 'checked' : '' }}>
                            <label class="form-check-label" for="gender1">
                                Male
                            </label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="gender" id="gender2" value="Female" {{$student_info_Byid->gender == 'Female' ? 'checked' : '' }}>
                            <label class="form-check-label" for="gender2">
                                Female
                            </label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="gender" id="gender3" value="Others">
                            <label class="form-check-label" for="gender2">
                                Others
                            </label>
                        </div>
                    </div>
                </div>
            </fieldset>

            <div class="form-group row">
                <div class="col-sm-2">Hobies</div>
                <div class="col-sm-10">
                    <div class="check form-check-inline">
                        <input class="form-check-input" name="hobies[]" type="checkbox" value = "football" id="gridCheck1"  {{ in_array('football', $student_info_Byid->hobies) ? 'checked' : '' }}>
                        <label class="form-check-label" for="gridCheck1">
                            Football
                        </label>
                    </div>
                    <div class="check form-check-inline">
                        <input class="form-check-input" name="hobies[]" type="checkbox" value = "cricket" id="gridCheck1" {{ in_array('cricket', $student_info_Byid->hobies) ? 'checked' : '' }}>
                        <label class="form-check-label" for="gridCheck1">
                            Cricket
                        </label>
                    </div>
                </div>
            </div>
            <br>
            <div class="form-group">
                Nationality
                
                
                    <select name ="nationality"class="form-control">
                        <option >Select Nationality</option>
                        <option value="Bangladesh" {{$student_info_Byid->nationality == 'Bangladesh' ? 'selected' : '' }}>Bangladesh</option>
                        <option value="UK" {{$student_info_Byid->nationality == 'UK' ? 'selected' : '' }}>United Kindom</option>
                        <option value="India" {{$student_info_Byid->nationality == 'India' ? 'selected' : '' }} > India</option>
                        <option value="pakistan" {{$student_info_Byid->nationality == 'pakistan' ? 'selected' : '' }}>Pakistan</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary">Add Student</button>
                </div>
            </div>
        </form>
    </div>
</x-master>