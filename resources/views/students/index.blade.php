<x-master>
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Dashboard</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group me-2">
                <a href="{{Route('students.create')}}"><button type="button" class="btn btn-success">Add Student Information</button></a>
            </div>

        </div>
    </div>


        @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if(session('message'))
<span class="text-success">{{ session('message') }}</span>
@endif

<table class="table">
  <thead>
    <tr>
      <th scope="col">Sl#</th>
      <th scope="col">Student Name</th>
      <th scope="col">Date of Birth Size</th>
      <th scope="col">Gender</th>
      <th scope="col">Hobies</th>
      <th scope="col">Nationality</th>
      <th scope="col">Action</th>
     
    </tr>
  </thead>
  <tbody>
    @foreach($student_info as $val)
    <tr>
      <th scope="row">{{$loop->iteration}}</th>
      <td>{{$val->dob}}</td>
      <td>{{$val->gender}}</td>
      <td>{{$val->hobies}}</td>
      <td>{{$val->nationality}}</td>
      <td>

      <a href="{{route('students.show', $val->id)}}"><button type="button" class="btn btn-primary">Show</button></a>

      
      <a href="{{route('students.edit', $val->id)}}"><button type="button" class="btn btn-primary">Edit</button></a>
      <a href="{{route('students.destroy', $val->id)}}"><button type="button" class="btn btn-primary">Delete</button></a>


      </td>
    </tr>
   @endforeach
  </tbody>
</table>
    
</x-master>