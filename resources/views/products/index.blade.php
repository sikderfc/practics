<x-master>
    <div style="text-align: right">
    <!-- <a href=""><button type="button" class="btn btn-secondary">Add Product</button></a> -->
    </div>
    @if(session('message'))
        <span class="text-success">{{ session('message') }}</span>
        @endif

<table class="table">
  <thead>
    <tr>
      <th scope="col">Sl#</th>
      <th scope="col">Product Name</th>
      <th scope="col">Product Size</th>
      <th scope="col">Status</th>
      <th scope="col">Action</th>
     
    </tr>
  </thead>
  <tbody>
    @foreach($get_all_product as $val)
    <tr>
      <th scope="row">{{$loop->iteration}}</th>
      <td>{{$val->product_name}}</td>
      <td>{{$val->size}}</td>
      <td>{{$val->status}}</td>
      <td>

      <a href="{{route('products.show', $val->id)}}"><button type="button" class="btn btn-primary">Show</button></a>

      
      <a href=""><button type="button" class="btn btn-primary">Edit</button></a>
      <a href="{{route('products.destroy', $val->id)}}"><button type="button" class="btn btn-primary">Delete</button></a>


      </td>
    </tr>
   @endforeach
  </tbody>
</table>
</x-master>