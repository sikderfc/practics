<x-master>
    <div style="text-align: right">
    <!-- <a href=""><button type="button" class="btn btn-secondary">Add Product</button></a> -->
    </div>

<table class="table">
  <thead>
    <tr>
      <th scope="col">Sl#</th>
      <th scope="col">Product Name</th>
      <th scope="col">Product Size</th>
      <th scope="col">Status</th>
      <th scope="col">Action</th>
     
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">{{$get_product->id}}</th>
      <td>{{$get_product->product_name}}</td>
      <td>{{$get_product->status}}</td>
      <td>{{$get_product->size}}</td>
      
    </tr>
  </tbody>
</table>
</x-master>