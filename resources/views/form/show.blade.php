
<x-master>

@if(session('message'))
        <span class="text-success">{{ session('message') }}</span>
        @endif
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Form</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group me-2">
                <a href="{{route('form.create')}}"><button type="button" class="btn btn-success">Add Form Information</button></a>
            </div>

        </div>
    </div>
    <table class="table">
  <thead>
  
        <tr>
      <th scope="col">#</th>
      <th scope="col">Email</th>
      <th scope="col">Last</th>
      <th scope="col">Handle</th>
      <th scope="col">Handle</th>
      <th scope="col">Handle</th>
      <th scope="col">Handle</th>
      <th scope="col">Handle</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td>{{$formList->email}}</td>
      <td>{{$formList->password}}</td>
      <td>{{$formList->checkbox}}</td>
      <td>{{$formList->sex}}</td>
      <td>{{$formList->selectbox}}</td>
      <td>{{$formList->is_active}}</td>      
    
    </tr>
    
  </tbody>
</table>
</x-master>
