<x-master>
    <p style="margin: 60px 60px"></p>
    <form action="{{Route('form.update',$formGetdata->id)}}" method="POST">
        @csrf
        <x-forms.input name="email" value="{{$formGetdata->email}}" type="text" id="staticEmail" class="bg-primary" title="Email2" placeholder="Enter Your Mail" />
        <br>
        <x-forms.input name="password" type="password" value="{{$formGetdata->password}}" id="password" title="Password" placeholder="Enter Your Mail" />
        <br> 
        @php
                $checklist = ['Is Active','cricket','others','others2','others3',];
               
                $checkedItem =$formGetdata->checkbox;

            @endphp


       <x-forms.checkbox :checklist="$checklist" :checkedItem="$checkedItem"/>
       
        <br>
        <br>
        <br>
        <br>
        <br>

        <x-forms.radio />
        <br>
        <x-forms.select />
        <br>        
        <div class="form-group row">
            <label for="staticEmail" class="col-sm-2 col-form-label"></label>
            <div class="col-sm-6">

                <div class="form-group">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="is_active" id="invalidCheck3">
                        <label class="form-check-label" for="invalidCheck3">
                            Agree to terms and conditions
                        </label>
                        <div class="invalid-feedback">
                            You must agree before submitting.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <x-forms.button />

    </form>
</x-master>