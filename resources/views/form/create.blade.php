<x-master>
    <p style="margin: 60px 60px"></p>
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Form</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group me-2">
                <a href="{{route('form.index')}}"><button type="button" class="btn btn-success">Home</button></a>
            </div>

        </div>
    </div>
    <form action="{{Route('form.store')}} " method="POST" enctype="multipart/form-data">
        @csrf
        <x-forms.input name="email" type="text" id="staticEmail" class="bg-primary" title="Email2" placeholder="Enter Your Mail" />
        <br>
        <x-forms.input name="password" type="password" id="password" title="Password" placeholder="Enter Your Mail" />
        <br>

       
        @php
                $checklist = ['Is Active','cricket','others','others2','others3',];
               

            @endphp


       <x-forms.checkbox :checklist="$checklist" name="checkbox[]" />
       
        <br>
        <br>
        <br>
        <br>
        <br>

        <x-forms.radio />
        <br>
        <x-forms.select />

        <br> 
        <x-forms.input name="imge" type="file" id="imge" title="Picture" />
       <br>
       <br>
              <div class="form-group row">
            <label for="staticEmail" class="col-sm-2 col-form-label"></label>
            <div class="col-sm-6">

                <div class="form-group">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="is_active" id="invalidCheck3">
                        <label class="form-check-label" for="invalidCheck3">
                            Agree to terms and conditions
                        </label>
                        <div class="invalid-feedback">
                            You must agree before submitting.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <x-forms.button />
        


    </form>
</x-master>