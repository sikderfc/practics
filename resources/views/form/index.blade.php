
<x-master>

@if(session('message'))
        <span class="text-success">{{ session('message') }}</span>
        @endif
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Form</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group me-2">
                <a href="{{route('form.create')}}"><button type="button" class="btn btn-success">Add Form Information</button></a>
            </div>

        </div>
    </div>
    <table class="table">
  <thead>
  
        <tr>
      <th scope="col">#</th>
      <th scope="col">Email</th>
      <th scope="col">password</th>
      <th scope="col">Handle</th>
      <th scope="col">Handle</th>
      <th scope="col">Handle</th>
      <th scope="col">Handle</th>
      <th scope="col">Handle</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    @foreach($formData as $val)
    <tr>
      <th scope="row">{{$loop->iteration}}</th>
      <td>{{$val->email}}</td>
      <td>{{$val->password}}</td>
      <td>{{$val->checkbox}}</td>
      <td>{{$val->sex}}</td>
      <td>{{$val->selectbox}}</td>
      <td>{{$val->is_active}}</td>      
    <td><a href="{{Route('form.edit',$val->id)}}"><button type="btn "  class="btn btn-primary">Edit </button></a>
        <a href="{{Route('form.show',$val->id)}}"><button type="btn " class="btn btn-success">show</button> </a>
        <a href="{{Route('form.destroy',$val->id)}}"><button type="btn " class="btn btn-danger"
        >Delete</button> </a></td>
    </tr>
    @endforeach
    
  </tbody>
</table>
</x-master>
