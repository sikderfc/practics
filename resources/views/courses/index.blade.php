<x-master>

    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Corse</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group me-2">
                <a href="{{route('courese.create')}}"><button type="button" class="btn btn-success">Add new Courese</button></a>
            </div>

        </div>
    </div>


        

<table class="table">
  <thead>
    <tr>
      <th scope="col">Sl#</th>
      <th scope="col">Courese Title</th>
      <th scope="col">category</th>
      <th scope="col">type  </th>
      <th scope="col">technology</th>
      <th scope="col">duraton</th>
      <th scope="col">start_form</th>
      <th scope="col">created_at</th>
      <th scope="col">updated_at</th>
      <th scope="col">Action</th>
     
    </tr>
  </thead>
  <tbody>
    @foreach ($courseInfo as $val)
        <tr>
      <th scope="row">{{$loop->iteration}}</th>
      <td>{{$val->title}}</td>
      <td>{{$val->category}}</td>
      <td>{{$val->type}}</td>
      <td>{{$val->technology}}</td>
      <td>{{$val->duraton}}</td>
      <td>{{$val->start_form}}</td>
      <td>{{$val->created_at}}</td>
      <td>{{$val->updated_at}}</td>

      <td>

      <a href="http://127.0.0.1:8000/student_show/1"><button type="button" class="btn btn-primary">Show</button></a>

      
      <a href="{{Route('courese.edit',$val->id)}}"><button type="button" class="btn btn-primary">Edit</button></a>
      <a href="http://127.0.0.1:8000/student_delete/1"><button type="button" class="btn btn-primary">Delete</button></a>


      </td>
    </tr>
    @endforeach
       
     </tbody>
</table>
    
</x-master>