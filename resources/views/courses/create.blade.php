<x-master>
    <div class="d-flex m-5 justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Add New Courese</h1>

    </div>
    <div class=" p-4 m-2" style="width:50%">
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
    <form action="{{Route('courese.store')}}" method="POST">
        @csrf  
        <x-forms.input name="title3" class=" form-control bg-secondary" /> 
        <x-forms.input name="title3" class=" form-control bg-primary" /> 
       
            <br>
            <div class="form-group">
                Cetegory
                
                
                    <select name ="category"class="form-control">
                        <option >Select Courese Category</option>
                        <option value="primary">Primary</option>
                        <option value="junior">Junior Secondary</option>
                        <option value="secondary">Secondary</option>
                        <option value="higher_secondary">Higher Secondary</option>
                        
                    </select>
                </div>
            </div>

            <fieldset class="form-group">
                <div class="row">
                    <legend class="col-form-label col-sm-2 pt-0">Type</legend>
                    <div class="col-sm-10">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="type" id="gender1" value="general" checked>
                            <label class="form-check-label" for="gender1">
                                General
                            </label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="type" id="gender2" value="science">
                            <label class="form-check-label" for="gender2">
                                Science
                            </label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="type" id="gender3" value="business">
                            <label class="form-check-label" for="gender2">
                                Buisness
                            </label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="type" id="gender3" value="humanitis">
                            <label class="form-check-label" for="gender2">
                               Humanitis
                            </label>
                        </div>
                    </div>
                </div>
            </fieldset>

            <div class="form-group row">
                <div class="col-sm-2">Technology</div>
                <div class="col-sm-10">
                    <div class="check form-check-inline">
                        <input class="form-check-input" name="technology[]" type="checkbox" value = "bangla" id="gridCheck1">
                        <label class="form-check-label" for="gridCheck1">
                            Bangla
                        </label>
                    </div>
                    <div class="check form-check-inline">
                        <input class="form-check-input" name="technology[]" type="checkbox" value = "english" id="gridCheck1">
                        <label class="form-check-label" for="gridCheck1">
                            English
                        </label>
                    </div>
                </div>
            </div>
            <br>
            <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label"> Duration</label>
                <div class="col-sm-10">
                    <input type="text" name ="duraton" class="form-control" id="inputEmail3" placeholder="Enter Your Duraton Time">
                </div>
            </div>
            
            
            <br>
            <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label"> Start from</label>
                <div class="col-sm-10">
                    <input type="date" name ="start_form" class="form-control" id="inputEmail3" placeholder="Enter Your title">
                </div>
            </div>
            <br>
            <br>

            <div class="form-group row">
                <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary">Add Course</button>
                </div>
            </div>
        </form>
    </div>
</x-master>