<x-master>
    <div class="d-flex m-5 justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Add New Courese</h1>

    </div>
    <div class=" p-4 m-2" style="width:50%">
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
    <form action="{{Route('courese.update',$courseInfo->id)}}" method="POST">
        @csrf   
        <div class="form-group row">
                <label for="inputEmail3" class="col-sm-3 col-form-label">Courese Title</label>
                <div class="col-sm-9">
                    <input type="text" value="{{$courseInfo->title}}" name ="title" class="form-control" id="inputEmail3" placeholder="Enter Your title">
                </div>
            </div>
            <br>
            <div class="form-group">
                Cetegory
                
                
                    <select name ="category"class="form-control">
                        <option >Select Courese Category</option>
                        <option value="primary"{{$courseInfo->category == 'primary' ? 'selected' :  ''}}>Primary</option>
                        <option value="junior" {{$courseInfo->category == 'junior' ? 'selected' :  ''}}>Junior Secondary</option>
                        <option value="secondary"{{$courseInfo->category == 'secondary' ? 'selected' :  ''}}>Secondary</option>
                        <option value="higher_secondary"{{$courseInfo->category == 'higher_secondary' ? 'selected' :  ''}}>Higher Secondary</option>
                        
                    </select>
                </div>
            </div>

            <fieldset class="form-group">
                <div class="row">
                    <legend class="col-form-label col-sm-2 pt-0">Type</legend>
                    <div class="col-sm-10">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="type" id="gender1" value="general" {{$courseInfo->type == 'general' ? 'checked' :  ''}}>
                            <label class="form-check-label" for="gender1">
                                General
                            </label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="type" id="gender2" value="science"{{$courseInfo->type == 'science' ? 'checked' :  ''}}>
                            <label class="form-check-label" for="gender2">
                                Science
                            </label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="type" id="gender3" value="business"{{$courseInfo->type == 'business' ? 'checked' :  ''}}>
                            <label class="form-check-label" for="gender2">
                                Buisness
                            </label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="type" id="gender3" value="humanitis"{{$courseInfo->type == 'humanitis' ? 'checked' :  ''}}>
                            <label class="form-check-label" for="gender2">
                               Humanitis
                            </label>
                        </div>
                    </div>
                </div>
            </fieldset>

            <div class="form-group row">
                <div class="col-sm-2">Technology</div>
                <div class="col-sm-10">
                    <div class="check form-check-inline">
                        <input class="form-check-input" name="technology[]" type="checkbox" value = "bangla"  id="gridCheck1" {{ in_array('bangla', $courseInfo->technology)? 'checked' : ''}}>
                        <label class="form-check-label" for="gridCheck1">
                            Bangla
                        </label>
                    </div>
                    <div class="check form-check-inline">
                        <input class="form-check-input" name="technology[]" type="checkbox" value = "english" id="gridCheck1" {{ in_array('english', $courseInfo->technology)? 'checked' : ''}}>
                        <label class="form-check-label" for="gridCheck1">
                            English
                        </label>
                    </div>
                </div>
            </div>
            <br>
            <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label"> Duration</label>
                <div class="col-sm-10">
                    <input type="text" name ="duraton" value="{{$courseInfo->duraton}}" class="form-control" id="inputEmail3" placeholder="Enter Your Duraton Time">
                </div>
            </div>
            
            
            <br>
            <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label"> Start from</label>
                <div class="col-sm-10">
                    <input type="date" value="{{$courseInfo->start_form}}" name ="start_form" class="form-control" id="inputEmail3" placeholder="Enter Your title">
                </div>
            </div>
            <br>
            <br>

            <div class="form-group row">
                <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary">Update Course</button>
                </div>
            </div>
        </form>
    </div>
</x-master>